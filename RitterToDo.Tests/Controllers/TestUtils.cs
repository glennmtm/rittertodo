﻿ ﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RitterToDo.Models;

namespace RitterToDo.Tests.Controllers
{
  public class TestUtils
  {
    public IEnumerable<IEnumerable<ToDo>> GetStarredCases()
    {
      return new[]
             {
                 new[]
                 {
                     new ToDo() { Starred = true }, 
                     new ToDo() { Starred = false },
                     new ToDo() { Starred = true }, 
                     new ToDo() { Starred = false },
                 }
                 ,
                 new[]
                 {
                     new ToDo() { Starred = true }, 
                     new ToDo() { Starred = false },
                 }
                 ,
                 new[]
                 {
                     new ToDo() { Starred = true }, 
                     new ToDo() { Starred = true },
                 }
                 ,
                 new[]
                 {
                     new ToDo() { Starred = false }, 
                     new ToDo() { Starred = false },
                 }
                 ,
                 new[]
                 {
                     new ToDo() { Starred = false }, 
                 }
                 ,
                 new[]
                 {
                     new ToDo() { Starred = true }, 
                 }
                 ,
                 new ToDo[] {},
             };
    }

    public IEnumerable<IEnumerable<ToDo>> GetOpenCases()
    {
      return new[]
            {
                new[]
                {
                    new ToDo() {Done = true},
                    new ToDo() {Done = false},
                    new ToDo() {Done = true},
                    new ToDo() {Done = false},
                },
                new[]
                {
                    new ToDo() { Done = true }, 
                    new ToDo() { Done = false },
                }
                ,
                new[]
                {
                    new ToDo() { Done = true }, 
                    new ToDo() { Done = true },
                }
                ,
                new[]
                {
                    new ToDo() { Done = false }, 
                    new ToDo() { Done = false },
                }
                ,
                new[]
                {
                    new ToDo() { Done = false }, 
                }
                ,
                new[]
                {
                    new ToDo() { Done = true }, 
                }
                ,
                new ToDo[] {},

            };
    }
  }
}