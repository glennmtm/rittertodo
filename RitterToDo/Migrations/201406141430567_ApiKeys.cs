namespace RitterToDo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApiKeys : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserApiKeysModel",
                c => new
                    {
                        ApiKey = c.Guid(nullable: false),
                        OwnerId = c.String(nullable: false, maxLength: 128),
                        AppName = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.ApiKey)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerId, cascadeDelete: true)
                .Index(t => t.OwnerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserApiKeysModel", "OwnerId", "dbo.AspNetUsers");
            DropIndex("dbo.UserApiKeysModel", new[] { "OwnerId" });
            DropTable("dbo.UserApiKeysModel");
        }
    }
}
